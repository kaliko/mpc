mpc (0.33-1) unstable; urgency=medium

  * New upstream version 0.33
  * Declare compliance with Debian Policy 4.4.1

 -- Florian Schlichting <fsfs@debian.org>  Thu, 07 Nov 2019 16:06:39 +0800

mpc (0.32-1) unstable; urgency=medium

  * New upstream version 0.32
  * Drop patches cherry-picked from upstream
  * Declare compliance with Debian Policy 4.4.0

 -- Florian Schlichting <fsfs@debian.org>  Tue, 16 Jul 2019 23:12:38 +0200

mpc (0.31-1) unstable; urgency=medium

  * New upstream version 0.31
  * Ensure at least meson 0.47
  * Cherry-pick 0001-fix-typo-procedes-proceeds.patch and
    0002-Fix-race-condition-with-sphinx-and-parallel-builds.patch from
    upstream (closes: #908756)
  * Update packaging copyright years
  * Switch to debhelper-compat, level 12
  * Declare compliance with Debian Policy 4.3.0

 -- Florian Schlichting <fsfs@debian.org>  Mon, 07 Jan 2019 23:22:03 +0100

mpc (0.30-1) unstable; urgency=medium

  * New upstream version 0.30
  * mpc.1 path no longer needs to be fixed
  * Drop rm_conffile bits as all upgrades from pre 0.27-2 versions need to go
    through 0.28 in stable

 -- Florian Schlichting <fsfs@debian.org>  Mon, 07 May 2018 22:46:07 +0200

mpc (0.29-2) unstable; urgency=medium

  * Team upload

  [ Florian Schlichting ]
  * Move packaging to salsa.d.o

  [ Simon McVittie ]
  * Standards-Version: 4.1.4 (no changes required)

 -- Simon McVittie <smcv@debian.org>  Mon, 30 Apr 2018 00:09:38 +0100

mpc (0.29-1) unstable; urgency=medium

  * New upstream version 0.29
  * Update debian/copyright: autotools dropped, years
  * Switch build from autotools to meson
  * Bump dh compat to level 11
  * Declare compliance with Debian Policy 4.1.3
  * Update for files moved from doc/ to contrib/, build manpage
  * Enable all hardening buildflags

 -- Florian Schlichting <fsfs@debian.org>  Mon, 12 Feb 2018 21:48:32 +0100

mpc (0.28-1) unstable; urgency=medium

  * Import Upstream version 0.28
  * Update debian/copyright, adding a paragraph for m4/ax_require_defined.m4
  * Make libmpdclient-dev dependency versioned (>= 2.9)
  * Declare compliance with Debian Policy 3.9.8
  * Use secure URIs

 -- Florian Schlichting <fsfs@debian.org>  Tue, 30 Aug 2016 20:27:48 +0200

mpc (0.27-2) unstable; urgency=medium

  * Add check-m4-subunit.patch: link to libsubunit (closes: #798279)
  * Install bash completion into proper path
    + switch to using dh_bash-completion
    + call rm_conffile on the obsolete file

 -- Florian Schlichting <fsfs@debian.org>  Wed, 09 Sep 2015 15:57:26 +0200

mpc (0.27-1) unstable; urgency=medium

  * Import Upstream version 0.27 (closes: #507066)
  * Update copyright years, adding license for m4/check.m4
  * Refresh our doc patch
  * Declare compliance with Debian Policy 3.9.6 (no changes)
  * Enable unit tests, sort build dependencies

 -- Florian Schlichting <fsfs@debian.org>  Tue, 02 Jun 2015 23:29:10 +0200

mpc (0.26-1) unstable; urgency=medium

  * Import Upstream version 0.26
  * Bump copyright years
  * Add upstream-signing-key.asc and pgpsigurlmangle option to debian/watch to
    enable automatic signatur checks on the upstream source

 -- Florian Schlichting <fsfs@debian.org>  Tue, 03 Jun 2014 20:33:00 +0200

mpc (0.25-1) unstable; urgency=low

  * Import Upstream version 0.25
  * Switch to .tar.xz upstream tarball
  * Update upstream copyright statement
  * Refresh Makefile.am.patch

 -- Florian Schlichting <fsfs@debian.org>  Thu, 14 Nov 2013 15:01:57 +0100

mpc (0.24-1) unstable; urgency=low

  * Import Upstream version 0.24
  * Bump years of FSF copyright
  * Declare compliance with Debian Policy 3.9.5
  * Drop configure.ac.patch, hyphen-as-minus-sign.patch: applied upstream
  * Update Vcs-* fields to their canonical value

 -- Florian Schlichting <fsfs@debian.org>  Thu, 07 Nov 2013 22:35:20 +0100

mpc (0.23-1) unstable; urgency=low

  [ Alexander Wirt ]
  * Remove myself from the list of uploaders

  [ Florian Schlichting ]
  * Import Upstream version 0.23 (closes: #631723, #608957)
  * Update Homepage and watch file
  * Bump Standards-Version to 3.9.4 (use copyright-format 1.0)
  * Bump debhelper compatibility to level 9
  * Switch to source format 3.0 (quilt)
  * Switch from cdbs to dh with autoreconf
  * Don't link against libnsl unnecessarily (configure.ac.patch)
  * Ship upstream's mppledit in examples and remove our own copy
  * New hyphen-as-minus-sign.patch
  * Add myself to Uploaders

 -- Florian Schlichting <fsfs@debian.org>  Thu, 11 Apr 2013 01:04:49 +0200

mpc (0.22-1) unstable; urgency=low

  * [4f042d4] Imported Upstream version 0.22
  * [4703eb6] Bump standards version
  * [a9389a9] Remove article in synopsis

 -- Alexander Wirt <formorer@debian.org>  Wed, 27 Jun 2012 08:15:41 +0200

mpc (0.20-2) unstable; urgency=low

  * [e462b4e] Install bash completion again (Closes: #616625)

 -- Alexander Wirt <formorer@debian.org>  Mon, 07 Mar 2011 08:15:21 +0100

mpc (0.20-1) unstable; urgency=low

  * Update maintainers and vcs headers
  * Bump standards version (no changes)
  * Add watchfile
  * Imported Upstream version 0.20
    - search filename works again (Closes: #570725)
  * Don't install mppledit twice (Closes: #576084)

 -- Alexander Wirt <formorer@debian.org>  Sun, 20 Feb 2011 23:39:14 +0100

mpc (0.19-2) unstable; urgency=low

  * Update URL in description
  * Fix debhelper build-deps (we use v5 for now)
  * Bump Standards-Version

 -- Decklin Foster <decklin@red-bean.com>  Mon, 04 Jan 2010 15:18:03 -0500

mpc (0.19-1) unstable; urgency=low

  * New upstream release

 -- Decklin Foster <decklin@red-bean.com>  Mon, 04 Jan 2010 15:18:03 -0500

mpc (0.17-1) unstable; urgency=low

  * New upstream release

 -- Decklin Foster <decklin@red-bean.com>  Fri, 21 Aug 2009 17:29:04 -0400

mpc (0.16-1) unstable; urgency=low

  * New upstream release
  * Use debhelper 5 for now.

 -- Decklin Foster <decklin@red-bean.com>  Thu, 28 May 2009 11:22:07 -0400

mpc (0.15-1) unstable; urgency=low

  * New upstream release
  * Update copyright file
  * Policy 3.8.0

 -- Decklin Foster <decklin@red-bean.com>  Thu, 05 Feb 2009 11:44:54 -0500

mpc (0.14-1) unstable; urgency=low

  * New upstream release (Closes: #358618, #502578, #495430)
  * Include playstream example script (Closes: #357157)

 -- Decklin Foster <decklin@red-bean.com>  Mon, 22 Dec 2008 19:46:42 -0500

mpc (0.12.1-1) unstable; urgency=low

  * New upstream release (Closes: #402579, #408388)
    - No longer uses MAXPATHLEN (Closes: #394731)

 -- Decklin Foster <decklin@red-bean.com>  Tue, 27 Mar 2007 20:48:37 -0400

mpc (0.11.2-2) unstable; urgency=low

  * Adopting package.

 -- Decklin Foster <decklin@red-bean.com>  Sat, 27 May 2006 19:51:23 -0400

mpc (0.11.2-1) unstable; urgency=low

  * New upstream: small bug fixes and features
  * Updated manpage (upstream) Closes: #295029
  * update email addresses, URL, documentation and Standards-Version
  * Acknowledge NMU

 -- Eric Wong <eric@petta-tech.com>  Sat, 12 Mar 2005 02:25:23 -0800

mpc (0.11.1-2.1) unstable; urgency=low

  * Non-maintainer upload (Sponsor for Eric)

 -- Richard A. Hecker <hecker@debian.org>  Fri, 30 Jul 2004 09:05:34 -0700

mpc (0.11.1-2) unstable; urgency=low

  * remove empty/unused directories

 -- Eric Wong <eric@petta-tech.com>  Sat, 19 Jun 2004 22:00:33 -0700

mpc (0.11.1-1) unstable; urgency=low

  * new upstream

 -- Eric Wong <eric@petta-tech.com>  Fri, 18 Jun 2004 15:09:18 -0700

mpc (0.11.0-1) unstable; urgency=low

  * new upstream
  * updated mppledit + new examples from upstream

 -- Eric Wong <eric@petta-tech.com>  Thu, 17 Jun 2004 23:17:34 -0700

mpc (0.10.3-4) unstable; urgency=low

  * re-add mppledit to examples

 -- Eric Wong <eric@petta-tech.com>  Mon, 17 May 2004 22:19:44 -0700

mpc (0.10.3-3) unstable; urgency=low

  * update copyright to include 2004

 -- Eric Wong <eric@petta-tech.com>  Mon, 19 Apr 2004 02:21:52 -0700

mpc (0.10.3-2) unstable; urgency=low

  * take out Arch directories in the diff.gz

 -- Eric Wong <eric@petta-tech.com>  Tue,  6 Apr 2004 14:44:03 -0700

mpc (0.10.3-1) unstable; urgency=low

  * new upstream
  * document customizations in debian/rules

 -- Eric Wong <eric@petta-tech.com>  Fri,  2 Apr 2004 21:16:33 -0800

mpc (0.10.2-1) unstable; urgency=low

  * new upstream

 -- Eric Wong <eric@petta-tech.com>  Wed, 24 Mar 2004 23:04:54 -0800

mpc (0.10.1-1) unstable; urgency=low

  * new upstream

 -- Eric Wong <eric@petta-tech.com>  Sun,  7 Mar 2004 10:16:06 -0800

mpc (0.10.0-2) unstable; urgency=low

  * update mppledit in /usr/share/doc/mpc/examples to allow sorting:
    mppledit sort

 -- Eric Wong <eric@petta-tech.com>  Sat,  6 Mar 2004 15:01:53 -0800

mpc (0.10.0-1) unstable; urgency=low

  * new upstream

 -- Eric Wong <eric@petta-tech.com>  Tue,  2 Mar 2004 21:00:14 -0800

mpc (0.9.2-2) unstable; urgency=low

  * updated Description a bit
  * updated to Standards-Version 3.6.1
  * bash completion installed to /etc/bash_completion.d
  * new maintainer

 -- Eric Wong <eric@petta-tech.com>  Sun, 22 Feb 2004 17:27:13 -0800

mpc (0.9.2-1) unstable; urgency=low

  * Updated to 0.9.2.
  * Switch to using cdbs.

 -- Warren Dukes (aka shank) <shank@mercury.chem.pitt.edu>  Tue, 28 Oct 2003 22:10:00 -0400

mpc (0.9.1-1) unstable; urgency=low

  * Updated to 0.9.1.

 -- Warren Dukes (aka shank) <shank@mercury.chem.pitt.edu>  Tue, 06 Sep 2003 22:10:00 -0400

mpc (0.9.0-1) unstable; urgency=low

  * Initial Release.

 -- Warren Dukes <shank@mercury.chem.pitt.edu>  Tue, 29 Aug 2003 20:07:00 -0400

mpc (0.8.1-1) unstable; urgency=low

  * Initial Release.

 -- Warren Dukes <shank@mercury.chem.pitt.edu>  Tue, 12 Aug 2003 19:14:01 -0400
